package com.test_scripts;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.ApplicationUtiils.HomePageUtill;
import com.ApplicationUtiils.LoginUtill;
import com.ApplicationUtiils.PostKnowledgeUtill;
import com.listener.RetryAnalyzer;

public class HomePageTestScript extends TestBase {
	
	HomePageUtill homePageUtiiObj;
	PostKnowledgeUtill postKnowledgeUtillObj;
	
	
	@Test(testName = "verfiy_Post_count_On_DashBoard_Page" ,retryAnalyzer = RetryAnalyzer.class )
	public void verifyPostCount()
	{
		int postCount = pageObj.getInstance(LoginUtill.class).Dologin().getPostCount();
		//postCount = homePageUtiiObj.getPostCount();
		System.out.println("post Count "+postCount);
		Assert.assertEquals(postCount, 60);
	}
	@Test(testName = "verfiy_Postknowledge_Page_from_DashBoard_Page")
	public void verifyPostKnowledge()
	{
		postKnowledgeUtillObj = pageObj.getInstance(LoginUtill.class).Dologin().clickCreatePost();
		String pagetitle =  postKnowledgeUtillObj.getPageTitle();
		System.out.println(pagetitle);
		
	}

}
