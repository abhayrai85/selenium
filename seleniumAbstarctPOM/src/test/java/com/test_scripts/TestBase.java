package com.test_scripts;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;

import com.pages.BasePage;
import com.pages.Page;
import com.utills.Common_function;

public class TestBase {

	public Common_function cfObj;
	public static WebDriver driver;
	public Page pageObj;

	@Parameters({ "browser", "appUrl" })
	@BeforeMethod
	public void setUp(String browser, String appUrl ) {
		try {
			cfObj = new Common_function();
			driver = cfObj.commonStartAndOpenURLBrowser(appUrl, browser, "prod");
			Thread.sleep(10000);
			pageObj = new BasePage(driver);
			

		} catch (Exception e)

		{

		}

	}

	@AfterMethod

	public void tearDown() {

		if(driver!=null)
		{
			driver.quit();
			
		}
		
	}

}
