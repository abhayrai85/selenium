package com.ApplicationUtiils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.pages.PostKnowledgePage;

public class PostKnowledgeUtill extends PostKnowledgePage {

	PostKnowledgePage postKnowledgeObj;
	
	public PostKnowledgeUtill(WebDriver driver) {

		super(driver);
		postKnowledgeObj = new PostKnowledgePage(driver);
		postKnowledgeObj.waitForElementToPresent(By.cssSelector(".box-panel>h2"));
		PageFactory.initElements(driver, postKnowledgeObj);
		
	}

}
