package com.ApplicationUtiils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import com.pages.HomePage;

public class HomePageUtill extends HomePage {

	HomePage homePage;

	public HomePageUtill(WebDriver driver) {
		super(driver);
		// wait for page to be open
		homePage = new HomePage(driver);
		homePage.waitForElementToPresent(By.cssSelector(".categories_tittle"));
		PageFactory.initElements(driver, homePage);
	}

	public String getUserText() {
		return homePage.getPageHeader(homePage.getTextUserName());
	}

	public int getPostCount() {
		int postCount = homePage.getPost().size();
		return postCount;
	}
	
	
	public PostKnowledgeUtill clickCreatePost()
	{
		homePage.getCreatePost().click();
		
		return homePage.getInstance(PostKnowledgeUtill.class);
	}
}
