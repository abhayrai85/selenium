package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class BasePage extends Page{

	public BasePage(WebDriver driver) {
		super(driver);
	}

	@Override
	public String getPageTitle() {
		// TODO Auto-generated method stub
		return driver.getTitle();
	}

	@Override
	public String getPageHeader(WebElement ele) {
		return ele.getText().toString();
	}

	@Override
	public void waitForElementToPresent(By locator) {
		// TODO Auto-generated method stub
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		
	}

}
