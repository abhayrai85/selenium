package com.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage extends BasePage {

	public HomePage(WebDriver driver) {
		super(driver);
	}

	@FindBy(how = How.CSS, using = ".page-heading")
	private WebElement lblPageHeader;

	@FindBy(how = How.CSS, using = ".categories_tittle")
	private WebElement textUserName;

	@FindBy(how = How.CSS, using = ".chat_wrap>div")
	private List<WebElement> posts;
	
	@FindBy(how = How.CSS, using = ".create-post.post>a")
	private WebElement textboxCreatePost;

	public WebElement getLblPageHeader() {
		return lblPageHeader;
	}

	public WebElement getTextUserName() {
		return textUserName;
	}

	public List<WebElement> getPost() {
		return posts;
	}
	
	public WebElement getCreatePost() {
		return textboxCreatePost;
	}
}
