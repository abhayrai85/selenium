package com.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class LoginPage extends BasePage {

	public LoginPage(WebDriver driver) {
		super(driver);
	}
	@FindBy(how = How.ID, using = "email")
	private WebElement text_Login;
	@FindBy(how = How.ID, using = "password")
	private WebElement text_password;
	
	@FindBy(how = How.ID, using = "signinbtn")
	private WebElement btn_signIn;	
	
	public WebElement getText_Login() {
		return text_Login;
	}

	public WebElement getText_password() {
		return text_password;
	}

	public WebElement getBtn_signIn() {
		return btn_signIn;
	}
}
