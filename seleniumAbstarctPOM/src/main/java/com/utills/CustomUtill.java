package com.utills;

import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CustomUtill {
	public static JSONObject fetchKeyFromJSON(String filename, String keyfromJSON) {
		Object obj = null;
		try {
			System.out.println(System.getProperty("user.dir"));
			obj = new JSONParser().parse(new FileReader(System.getProperty("user.dir")+"/src/main/resources/" + filename));
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JSONObject jo = (JSONObject) obj;

		JSONObject objectFromJson = (JSONObject) jo.get(keyfromJSON);
		return objectFromJson;
	}
}
