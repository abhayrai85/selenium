package com.utills;

import java.io.IOException;
import java.net.URL;

import javax.xml.parsers.ParserConfigurationException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.xml.sax.SAXException;

public class Common_function {

	
	public WebDriver commonStartAndOpenURLBrowser(String baseURL, String browser, String environment)
			throws ParserConfigurationException, SAXException, IOException {

		DesiredCapabilities capability = null;
		WebDriver driver = null;
		String seleniumRemoteAddress = null;

		try {
			switch (environment) {
			case "prod":
				seleniumRemoteAddress = "http://localhost:4444/wd/hub";
				break;
			case "local":
				seleniumRemoteAddress = "http://localhost:4444/wd/hub";
				break;
			case "testapp":
				seleniumRemoteAddress = "http://localhost:4444/wd/hub";
				break;
			default:
				seleniumRemoteAddress = "http://localhost:4444/wd/hub";
				break;
			}
			System.out.println(seleniumRemoteAddress);
			if (browser.equalsIgnoreCase("firefox")) {

				System.out.println("firefox");
				capability = DesiredCapabilities.firefox();
				capability.setBrowserName("firefox");
				capability.setPlatform(org.openqa.selenium.Platform.WINDOWS);

			}
			if (browser.equalsIgnoreCase("iexplore")) {
				System.out.println("iexplore");
				capability = DesiredCapabilities.internetExplorer();
				capability.setCapability("nativeEvents", true);
				capability.setBrowserName("iexplore");
				capability.setPlatform(org.openqa.selenium.Platform.WINDOWS);
			}

			if (browser.equalsIgnoreCase("chrome")) {
				// String userAgent =
				// "Mozilla/5.0 (Linux; Android 4.0.3; HTC One X Build/IML74K)
				// AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile
				// Safari/535.19";
				/* String proxy = "example:80"; */
				/*
				 * org.openqa.selenium.Proxy p = new org.openqa.selenium.Proxy();
				 * p.setHttpProxy(proxy).setFtpProxy(proxy).setSslProxy(proxy);
				 */
				ChromeOptions co = new ChromeOptions();
				// co.addArguments("--user-agent=" + userAgent);

				co.addArguments("test-type");

				System.out.println("chrome");
				capability = DesiredCapabilities.chrome();
				//capability.setCapability("proxy", proxy);
				//capability.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
				// capability.setCapability(CapabilityType.PROXY, p);
				capability.setBrowserName("chrome");
				// capability.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				capability.setCapability(ChromeOptions.CAPABILITY, co);
				System.out.println(capability.getVersion());
			}
			if (browser.equalsIgnoreCase("safari")) {
				System.out.println("safari");
				capability = DesiredCapabilities.safari();
				capability.setBrowserName("safari");
				capability.setPlatform(org.openqa.selenium.Platform.WINDOWS);
				System.out.println(capability.getVersion());

			}
			if (browser.equalsIgnoreCase("android")) {
				/*
				 * SelendroidCapabilities capa = new SelendroidCapabilities(
				 * "com.yatra.base:3.0.2"); driver = new SelendroidDriver(capa);
				 */
			} else {

				driver = new RemoteWebDriver(new URL(seleniumRemoteAddress), capability);
				driver.navigate().to(baseURL);
				driver.manage().window().maximize();
				driver = new Augmenter().augment(driver);
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {

		}
		return driver;
	}
}
