package com.utills;

import org.json.simple.JSONObject;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class APIUtils {

	public APIResponse ap;

	public APIResponse postCall(String endPoint, Object postData) {
		try {
			RestAssured.baseURI = System.getProperty("baseUrl");
			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json");
			request.body(postData);
			Response response = request.post(endPoint);
			System.out.println(response.getHeaders());
			ap = new APIResponse(response);
		} catch (Exception e) {

		}
		return ap;
	}
	
	
	public APIResponse getCall(String endPoint, JSONObject postData) {
		try {
			RestAssured.baseURI = "https://app.vwo.com";
			RequestSpecification request = RestAssured.given();
			request.header("Content-Type", "application/json");
			request.body(postData.toString());
			Response response = request.post(endPoint);
			System.out.println(response.getHeaders());
			ap = new APIResponse(response);
			System.out.println(ap.getmessageCode());
			System.out.println(ap.getCode());
			System.out.println(ap.getResponseHeader());
			System.out.println(ap.getCookie());
			System.out.println("****Cookie****");
		} catch (Exception e) {

		}
		return ap;
	}
	
	
	public  APIResponse customGET(String endpoint, boolean isRequired, String xsrf) {

		RequestSpecification request = RestAssured.given();
		request.header("apikey", "ixiweb!2$");
		Response response = null;
		if (xsrf.equalsIgnoreCase("NA") && isRequired == false) {
			response = request.contentType(ContentType.JSON).get(endpoint);
		}
		else if (!xsrf.equalsIgnoreCase("NA") && !xsrf.equalsIgnoreCase("VWOCSRF") && !xsrf.isEmpty()) {
		}
		
		else if (xsrf.equalsIgnoreCase("someKey") && isRequired == true) {
		}

		APIResponse ap = new APIResponse(response);
		return ap;

	}
	
	
	


}
